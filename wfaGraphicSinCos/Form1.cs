﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfaGraphicSinCos
{
    public partial class Form1 : Form
    {
        private const int DOT = 4;

        public Form1()
        {
            InitializeComponent();

            this.BackgroundImageLayout = ImageLayout.None;
            this.Resize += (s, e) => DrawAll();
            this.Text += " : (Sin - красный, Cos - Зеленый, Tan - Синий)";

            DrawAll();

            // HW
            // При движении мышкой над графиком, показывать значение синуса, косинуса и тангенса
            // Ровная вертикальная линия, которая следует за мышкой
        }

        private void DrawAll()
        {
            var b = new Bitmap(this.ClientSize.Width, this.ClientSize.Height);
            var g = Graphics.FromImage(b);

            var grCountWave = 5; // Кол-во волн
            var grShiftY = b.Height / 2; // Смещение по Y
            var grHeight = grShiftY * 0.8; //Половина высоты графика
            var grWidthPI = Math.PI / (b.Width - 1); //Ширина отрезка PI в радианах

            // Рисуем ось x
            //g.DrawLine(new Pen(Color.Black), 0, grShiftY, b.Width, grShiftY);
            g.DrawLine(Pens.Black, 0, grShiftY, b.Width, grShiftY);

            for (int i = 1; i < grCountWave; i++)
            {
                var pointX = b.Width / grCountWave * i;
                g.DrawLine(Pens.Black, pointX, grShiftY - 5, pointX, grShiftY + 5);
                g.DrawString($"{i}π", new Font("", 10), new SolidBrush(Color.Black), pointX - 10, grShiftY + 5);
            }

            // Рисуем ось y
            g.DrawLine(Pens.Black, 0, 0, 0, b.Height);

            // Рисуем график
            float x;
            float y;
            for (int i = 0; i < b.Width; i++)
            {
                x = i;
                // график синуса
                y = (float)(grHeight * -Math.Sin(i * grCountWave * grWidthPI) + grShiftY);
                g.FillEllipse(new SolidBrush(Color.Red), x - DOT / 2, y - DOT / 2, DOT, DOT);

                // график синуса
                y = (float)(grHeight * -Math.Cos(i * grCountWave * grWidthPI) + grShiftY);
                g.FillEllipse(new SolidBrush(Color.Green), x - DOT / 2, y - DOT / 2, DOT, DOT);

                // график синуса
                y = (float)(grHeight * -Math.Tan(i * grCountWave * grWidthPI) + grShiftY);
                if (y > 0 && y < Width)
                    g.FillEllipse(new SolidBrush(Color.Blue), x - DOT / 2, y - DOT / 2, DOT, DOT);

                // график Волны
                y = (float)(grHeight * (-Math.Sin(i * 5 * grCountWave * grWidthPI) * i / grHeight / 3) + grShiftY);
                    g.FillEllipse(new SolidBrush(Color.Orange), x - DOT / 2, y - DOT / 2, DOT, DOT);
            }

            this.BackgroundImage = (Bitmap)b.Clone();
        }
    }
}
